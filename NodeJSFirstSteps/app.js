﻿var express = require('express3');
var routes = require('./routes');
var students = require('./routes/students.js');
var http = require('http');
var path = require('path');
var studentsData = require('./data/studentsData.js');
var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', students.main);
app.get('/searching', students.search);
app.get('/all', students.list);
app.get('/show/*', students.show);
app.get('/show_edit/*', students.show_edit);
app.get('/save', students.save);
app.get('/delete', students.delete);



http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});