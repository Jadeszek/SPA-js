﻿/**
    This is the data source for your application. In the real world, you would either get this data from a data base or
    from an API 
**/
exports.students_data = [
    {
        "id" : 1,
        "name": "student",
        "subjects": [   "a",
                        "b",
                        "c"
        ],
        "photo" : "http://domaingang.com/wp-content/uploads/2012/02/example.png"
    },
    {
        "id" : 2,
        "name": "Kamil Kamilowicz",
        "subjects": ["Analiza matematyczna",
        "Algebra",
        "Systemy Wbudowane"
        ],
        "photo" : "http://upload.wikimedia.org/wikipedia/commons/f/ff/Guy_Pearce_Cannes_2012.jpg"
    },
    {
        "id" : 3,
        "name": "Zenon Zenonowicz",
        "subjects": ["Programowanie Obiektowe",
        "Programowanie aplikacji w języku JavaScript",
        "Wychowanie Fizyczne"
        ],
        "photo" : "http://upload.wikimedia.org/wikipedia/commons/5/5a/Richard_K_Guy_2005.jpg  "
    },
    {
        "id" : 4,
        "name": "Eugeniusz Gienio",
        "subjects": ["Programowanie Obiektowe",
        "Wychowanie Fizyczne",
        "Algebra"
        ],
        "photo" : "http://upload.wikimedia.org/wikipedia/commons/5/5a/Richard_K_Guy_2005.jpg  "
    },
    {
        "id" : 5,
        "name": "Radosław Radziowski",
        "subjects": ["Grafika inżynierska",
        "Systemy czasu rzeczywistego",
        "Mechanika teoretyczna"
        ],
        "photo" : "http://orig10.deviantart.net/ef55/f/2012/022/e/a/pixel_guy_fawkes_by_tsundere_kko-d4nah0x.jpg"
    },
    {
        "id" : 6,
        "name": "Tomasz Tomkowicz",
        "subjects": ["Programowanie mikrokontrolerów",
        "Matematyka II",
        "Matematyka III"
        ],
        "photo" : "http://images.media-allrecipes.com/userphotos/250x250/01/07/80/1078019.jpg"
    },
    {
        "id" : 7,
        "name": "Grzegorz Brzęczyszczykiewicz",
        "subjects": ["Energetyka",
        "Oświetlenie",
        "Techniki wysokich napięć"
        ],
        "photo" : "http://images.media-allrecipes.com/userphotos/250x250/01/07/80/1078019.jpg"
    }
]

