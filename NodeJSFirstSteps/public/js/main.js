﻿$(function () {
    $('#search').on('keyup', function (e) {
            var parameters = { search: $(this).val() };
            $.get('/searching', parameters, function (data) {
                $('#results').html(data);
                $('.RES').click(function (event) {
                    $.get('/show/'+ $(this).attr('id'), null, function (data) {
                    $('#gallery').html('');
                    $('#results').html('');
                    $('#profile').html(data);
                    $('.EDIT').click(function (event) {
                        $.get('/show_edit/' + $(this).attr('id'), null, function (data) {
                            $('#gallery').html('');
                            $('#results').html('');
                            $('#profile').html(data);
                        });
                    });
                    });   
                });
            });    
    });
});


$(function () {
    $('#all').on('click', function (event) {
        //alert(7)
        $.get('/all', null, function (data) {
            $(':root').html(data);
        });
    });
});


$(function () {
    $('.STUDENT').on('click', function (event) {
        $.get('/show/' + $(this).attr('id'), null, function (data) {
            $('#gallery').html('');
            $('#results').html('');
            $('#profile').html(data);
            $('.EDIT').click(function (event) {
                $.get('/show_edit/' + $(this).attr('id'), null, function (data) {
                    $('#gallery').html('');
                    $('#results').html('');
                    $('#profile').html(data);
                });
            });
        });
    });
});

