﻿var students = require('../data/studentsData.js');

exports.main = function (req, res) {
  
    res.render('student', {
        students: {
            list: [],
        }
    });

}


exports.show = function (req, res) {
    
    var parts = req.originalUrl.split('/');
    var id = parts[parts.length - 1];
    
    function hasID(element, index, array) {
        console.log(element.id + "==" + id);
        return element.id == id
    }
    
    var result = students.students_data.filter(hasID);
    
    console.log(result[0]);
        

    res.render('profile', {
        student: result[0]
    });

}


exports.show_edit = function (req, res) {
    
    var parts = req.originalUrl.split('/');
    var id = parts[parts.length - 1];
    
    function hasID(element, index, array) {
        console.log(element.id + "==" + id);
        return element.id == id
    }
    
    var result = students.students_data.filter(hasID);
    
    console.log(result[0]);
    
    res.render('edit_profile', {
        student: result[0]
    });

}

exports.save = function (req, res) {
    
    var id = req.query.id
    var name = req.query.name
    var img = req.query.img

    console.log("Saving:\t id:" + id + "\tname:" + name + "\tphoto:" + img);
    
    var current_index = -1;
    
    for (i = 0; i < students.students_data.length; i++) {
        if (students.students_data[i].id == id) {
            current_index = i;
        }
    }

    
    students.students_data[current_index].name = name
    students.students_data[current_index].photo = img;

    res.render('student', {
        students: {
            list: students.students_data,
        }
    });

}

exports.delete = function (req, res) {
    
    var id = req.query.id
    
    console.log("Remove:\t id:" + id);
    
    var current_index = -1;
    
    for (i = 0; i < students.students_data.length; i++) {
        if (students.students_data[i].id == id) {
            current_index = i;
        }
    }
    
    students.students_data.splice(current_index, 1);

   
    res.render('student', {
        students: {
            list: students.students_data,
        }
    });

}



exports.list = function (req, res) {

    res.render('student', {
        students: {
            list: students.students_data,
        }
    });

}

exports.search = function (req, res){
    
    var val = req.query.search
    console.log("Searching for: " + val);
    
    console.log(students.students_data);    

    function hasName(element, index, array) {
        console.log(element.name + "==" + val);
        // return element.name == val
        if (val === "") {
            return false
        }
        
        if (val === " ") {
            return false
        }

        return element.name.toLowerCase().indexOf(val.toLowerCase()) > -1
    }
    
    var searched_students = students.students_data.filter(hasName);
    
    console.log("Results: " + searched_students.length);

    res.render('results', {
        results: searched_students
    });


}